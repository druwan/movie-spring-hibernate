package no.qux.demo.hibernate.model;

public class MessageContainer {
    private String message;

    public MessageContainer(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
